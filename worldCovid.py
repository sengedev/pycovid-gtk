import wx
import requests
from bs4 import BeautifulSoup
import json
import time


def getCountry(type):
    global req
    headers = {
        'User-Agent': 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/91.0.4472.114 '
                      'Safari/537.36 '
    }
    try:
        req = requests.get(url='https://ncov.dxy.cn/ncovh5/view/pneumonia_risks?from=dxy&link=&share=&source=',
                           headers=headers)
    except Exception:
        wx.MessageBox('网络连接失败')
        exit(1)
    req.encoding = 'utf8'
    html = req.text
    soup = BeautifulSoup(html, "html.parser")
    soup = soup.find('script', id='getListByCountryTypeService2true')
    data = str(soup).replace(
        '<script id="getListByCountryTypeService2true">try { window.getListByCountryTypeService2true = ', '')
    data = data.replace('}catch(e){}</script>', '')
    WorldData = json.loads(data)
    if type == 'country':
        CountryName = {'亚洲': [], '非洲': [], '欧洲': [], '北美洲': [], '南美洲': [], '大洋洲': [], '其他': []}
        for item in WorldData:
            CountryName[item['continents']].append(item['provinceName'])
        return CountryName
    elif type == 'covid':
        return WorldData


class WorldCovid(wx.Panel):
    def __init__(self, parent):
        wx.Panel.__init__(self, parent)
        CountryName = getCountry('country')
        self.continents_string = wx.StaticText(self, label='洲', pos=(50, 50))
        self.continents_choose = wx.ComboBox(self, pos=(100, 50), size=(150, 25),
                                             choices=['亚洲', '欧洲', '非洲', '北美洲', '南美洲', '大洋洲', '其他'],
                                             style=wx.TE_READONLY)
        self.continents_choose.Bind(wx.EVT_COMBOBOX, self.onChooseContinents)
        self.country_string = wx.StaticText(self, label='国家', pos=(270, 50))
        self.country_choose = wx.ComboBox(self, pos=(320, 50), size=(150, 25), choices=['请先选择洲'], style=wx.TE_READONLY)
        self.country_search = wx.Button(self, label='搜索', pos=(490, 45), size=(100, 35))
        self.country_search.Bind(wx.EVT_BUTTON, self.onClickSearch)
        self.time_label = wx.StaticText(self, label='查询时间', pos=(50, 170))
        self.time_output = wx.TextCtrl(self, pos=(110, 170), size=(480, 25),
                                       style=wx.TE_READONLY)  # 禁止手动输入
        self.current_label = wx.StaticText(self, label='现存确诊', pos=(50, 220))
        self.current_output = wx.TextCtrl(self, pos=(110, 220), size=(205, 25),
                                          style=wx.TE_READONLY)  # 禁止手动输入
        self.total_label = wx.StaticText(self, label='累计确诊', pos=(325, 220))
        self.total_output = wx.TextCtrl(self, pos=(385, 220), size=(205, 25),
                                        style=wx.TE_READONLY)  # 禁止手动输入
        self.cured_label = wx.StaticText(self, label='累计治愈', pos=(50, 270))
        self.cured_output = wx.TextCtrl(self, pos=(110, 270), size=(205, 25),
                                        style=wx.TE_READONLY)  # 禁止手动输入
        self.dead_label = wx.StaticText(self, label='累计死亡', pos=(325, 270))
        self.dead_output = wx.TextCtrl(self, pos=(385, 270), size=(205, 25),
                                       style=wx.TE_READONLY)  # 禁止手动输入
        self.current_label_inc = wx.StaticText(self, label='新增确诊', pos=(50, 320))
        self.current_output_inc = wx.TextCtrl(self, pos=(110, 320), size=(205, 25),
                                              style=wx.TE_READONLY)  # 禁止手动输入
        self.total_label_inc = wx.StaticText(self, label='累计新增', pos=(325, 320))
        self.total_output_inc = wx.TextCtrl(self, pos=(385, 320), size=(205, 25),
                                            style=wx.TE_READONLY)  # 禁止手动输入
        self.cured_label_inc = wx.StaticText(self, label='新增治愈', pos=(50, 370))
        self.cured_output_inc = wx.TextCtrl(self, pos=(110, 370), size=(205, 25),
                                            style=wx.TE_READONLY)  # 禁止手动输入
        self.dead_label_inc = wx.StaticText(self, label='新增死亡', pos=(325, 370))
        self.dead_output_inc = wx.TextCtrl(self, pos=(385, 370), size=(205, 25),
                                           style=wx.TE_READONLY)  # 禁止手动输入

    def onChooseContinents(self, event):
        ContinentChoice = self.continents_choose.GetValue()
        CountryName = getCountry('country')
        self.country_choose.SetItems(CountryName[ContinentChoice])
        self.country_choose.SetValue(CountryName[ContinentChoice][0])
        self.time_output.SetValue(time.strftime('%Y-%m-%d_%H:%M:%S', time.localtime()))
    def onClickSearch(self, event):

        country = self.country_choose.GetValue()
        if self.continents_choose.GetValue() == '':
            wx.MessageBox('请先选择洲和国家！')
            return
        data = getCountry('covid')
        for item in data:
            if country == item['provinceName']:
                self.current_output.SetValue(str(item['currentConfirmedCount']))
                self.total_output.SetValue(str(item['confirmedCount']))
                self.dead_output.SetValue(str(item['deadCount']))
                self.cured_output.SetValue(str(item['curedCount']))
                self.current_output_inc.SetValue(str(item['incrVo']['currentConfirmedIncr']))
                self.total_output_inc.SetValue(str(item['incrVo']['confirmedIncr']))
                self.dead_output_inc.SetValue(str(item['incrVo']['deadIncr']))
                self.cured_output_inc.SetValue(str(item['incrVo']['curedIncr']))
                break