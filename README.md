# pycovid-GTK

#### 介绍
使用Python语言编写的新冠肺炎疫情小工具。

#### 软件说明
- 本软件使用Linux开发，Windows或macOS运行可能会遇到部分问题，不过核心功能可以使用。
- 本软件使用爬虫的方式获取丁香园官网的新冠肺炎疫情数据，可以查看国某个地区或某个国家的新冠肺炎疫情数据，数据可以实时更新而不用实时查看该软件。
- 本软件使用GPL3 的协议进行开源，允许二次修改，但是修改后的源码需要开源。
- 本软件仅提供源代码，如需使用，请自行编译该程序。


#### 安装教程

安装Python 3，如果你的电脑没有安装，可以访问[Python官网](https://www.python.org/downloads/)进行下载。
下载以后，使用pip命令安装以下库
|名称|功能|导入方法|
|-|-|-|
|beautifulsoup4|处理HTML代码|bs4|
|requests|发送请求|requests|
|wxpython|Python图形化|wx|
> 安装完成以后，cd到应用程序目录，然后运行`python generateGUI.py`即可运行程序。

如果不会使用pip，可以使用pycharm手动导入以下库。
PyCharm下载链接如下
- [Windows](https://www.jetbrains.com/pycharm/download/#section=windows)
- [macOS](https://www.jetbrains.com/pycharm/download/#section=mac)
- [Linux](https://www.jetbrains.com/pycharm/download/#section=linux)
